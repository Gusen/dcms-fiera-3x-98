<?php

$forum = mysql_fetch_object(mysql_query('SELECT * FROM `forum` WHERE `id` = '.intval($_GET['forum'])));
$razdel = mysql_fetch_object(mysql_query('SELECT * FROM `forum_razdels` WHERE `id_forum` = '.$forum->id.' AND `id` = '.intval($_GET['razdel'])));

if (!$razdel || !$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = (isset($_GET['create_theme'])) ? output_text($razdel->name, 1, 1, 0, 0, 0).' - создание темы' : 'Раздел - '.output_text($razdel->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut();
 
    if (isset($user) && isset($_GET['create_theme'])) {
        include_once 'action/create_theme.php'; // Создание темы.
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?
    if (isset($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    if (isset($user) && $razdel->type == 0 || ($razdel->type == 1) && $user['group_access'] > 2) {
        ?>
        <div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/create_theme.html'>Создать тему</a></div>
        <?
    }
    $k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id), 0);
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Тем в этом разделе ещё не создавали.</div>
        <?
    } else {
        ?>
        <table class = 'post'>
            <?
            $themes = mysql_query('SELECT * FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id.' ORDER BY `type` DESC, `time_post` DESC LIMIT '.$start.', '.$set['p_str']);
            while ($theme = mysql_fetch_object($themes)) {
                $creater = mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$theme->id_user));
                $count_posts = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id), 0);
                $hide = (user_access('forum_post_ed')) ? NULL : '`hide` = "0" AND';
                $last_post = mysql_fetch_object(mysql_query('SELECT `id`, `hide`, `id_user`, `time` FROM `forum_posts` WHERE '.$hide.' `id_theme` = '.$theme->id.' ORDER BY `id` DESC'));
                if ($last_post && $last_post->id_user != 0) {
                    $who = mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$last_post->id_user));
                    $who_id = $who ? $who->id : 0;
                    $who_nick = $who ? $who->nick : 'Система';
                } elseif ($last_post && $last_post->id_user == 0) {
                    $who_id = 0;
                    $who_nick = 'Система';
                }
                if ($theme->reason_close != NULL) {
                    $type = '_close';
                } elseif ($theme->type == 1) {
                    $type = '_up';
                } else {
                    $type = NULL;
                }
                ?>
                <tr>
                    <td class = 'icon14'>
                        <img src = '<?= FORUM ?>/icons/theme<?= $type ?>.png' alt = '' <?= ICONS ?> />
                    </td>
                    <td class = 'p_t'>
                        <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a> (<?= $count_posts ?>) - <?= vremja($theme->time) ?>
                    </td>
                </tr>
                <tr>
                    <?
                    if ($last_post) {
                        ?>
                        <td class = 'p_m' colspan = '2'>
                            <b>Автор: <a href = '/info.php?id=<?= $theme->id_user ?>'><?= $creater->nick ?></a> | Посл.:</b> <a href = '/info.php?id=<?= $who_id ?>'><?= $who_nick ?></a> (<?= vremja($last_post->time) ?>)
                        </td>
                        <?
                    } else {
                        $continue = (mb_strlen($theme->description) > 150) ? '...' : NULL;
                        ?>
                        <td class = 'p_m' colspan = '2'>
                            <?= output_text(mb_substr($theme->description, 0, 150), 1, 1, 0, 1, 1).$continue ?>
                        </td>
                        <?
                    }
                    ?>
                </tr>
                <?
            }
            ?>
        </table>
        <?
        if ($k_page > 1) {
            str(FORUM.'/'.$forum->id.'/'.$razdel->id.'/', $k_page, $page);
        }
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?
}

?>